package ru.klaw.restlog.service;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.model.LogFilter;
import ru.klaw.restlog.model.Severity;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;

class LogFileParserTest {

    private FileService service = Mockito.mock(FileService.class);
    @SuppressWarnings("ConstantConditions")
    private final File file = new File(this.getClass().getClassLoader().getResource("service/servers/ServerName/logs/ServerName.log").getPath());

    @Test
    void findLog() {
        final FileService.FileMetaData[] fileMetaData = {new FileService.FileMetaData(file)};
        Mockito.when(service.getLogFileArray(eq("test1"))).thenReturn(fileMetaData);
        final LogFileParser logFileParser = new LogFileParser(service);

        final Log[] logs = logFileParser.findLog(new LogFilter(), "test1", 100, 0, 5000, null);

        assertEquals(3,logs.length);
        final Log line = logs[0];
        assertEquals("25.05.2020 10:01:00,000 MSK", line.getTimestamp());
        assertEquals(Severity.EMERGENCY, line.getSeverity());
        assertEquals("Subsystem", line.getSubsystem());
        assertEquals("MachineName", line.getMachine());
        assertEquals("ServerName", line.getServer());
        assertEquals("ThreadID", line.getThreadId());
        assertEquals("<User ID>", line.getUserId());
        assertEquals("", line.getTransactionId());
        assertEquals("3fa7a048-e9cb-420a-a7de-9df95f3a8fbe-00000004", line.getDiagnosticContextId());
        assertEquals("1590390060000", line.getRawTime());
        assertEquals("BEA-111111", line.getMessageId());
        assertEquals("Text1", line.getText());
    }

    @Test
    void findLogWithFilterBySeverity() {
        final FileService.FileMetaData[] fileMetaData = {new FileService.FileMetaData(file)};
        Mockito.when(service.getLogFileArray(eq("test2"))).thenReturn(fileMetaData);
        final LogFileParser logFileParser = new LogFileParser(service);
        final LogFilter filter = new LogFilter();
        filter.setSeverity(Severity.EMERGENCY);
        final Log[] logs = logFileParser.findLog(filter, "test2", 100, 0, 5000, null);

        assertEquals(1,logs.length);
    }

    @Test
    void findLogSkipFirstLineTest() {
        final FileService.FileMetaData[] fileMetaData = {new FileService.FileMetaData(file)};
        Mockito.when(service.getLogFileArray(eq("test3"))).thenReturn(fileMetaData);
        final LogFileParser logFileParser = new LogFileParser(service);
        final LogFilter filter = new LogFilter();
        final Log[] logs = logFileParser.findLog(filter, "test3", 100, 1, 5000, null);

        assertEquals(2,logs.length);
    }
}