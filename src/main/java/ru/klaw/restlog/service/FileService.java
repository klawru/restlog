package ru.klaw.restlog.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.klaw.restlog.model.Log;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public class FileService {
    private final String domainHome;
    private final String serverName;

    @Autowired
    public FileService(EnvironmentService environment) {
        domainHome = environment.getSystemEnvironment("DOMAIN_HOME");
        serverName = environment.getSystemEnvironment("SERVER_NAME");
    }

    public FileMetaData[] getLogFileArray(String serverName) {
        serverName = Objects.isNull(serverName) || serverName.isEmpty() ? this.serverName : serverName;
        final File file = getPathToLog(serverName).toFile();
        final File[] files = file.listFiles((FilenameFilter) new PrefixFileFilter(serverName));
        if(files!=null) {
            Arrays.sort(files,Comparator.comparing(File::getName, Comparator.naturalOrder()));
            Arrays.sort(files,1,files.length,Comparator.comparing(File::getName, Comparator.reverseOrder()));
            return Stream.of(files).map(FileMetaData::new).toArray(FileMetaData[]::new);
        }
        return new FileMetaData[0];
    }

    private Path getPathToLog(String serverName) {
        return FileSystems.getDefault().getPath(domainHome, "servers", serverName, "logs");
    }

    @Data
    public static class FileMetaData {
        File file;
        Instant start;
        public FileMetaData(File file) {
            this.file = file;
            try (RandomAccessFile file1 = new RandomAccessFile(file, "r")) {
                final Log line = WebLogicLogParser.parseLine(file1.readLine());
                start=Instant.ofEpochMilli(Long.parseLong(line.getRawTime()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
