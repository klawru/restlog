package ru.klaw.restlog.service;

import org.springframework.stereotype.Service;

@Service
public class EnvironmentService {

    public String getSystemEnvironment(String name){
        return System.getenv(name);
    }
}
