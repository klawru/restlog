package ru.klaw.restlog.model;

/**
 * The severity attribute of a WebLogic Server log message indicates the potential impact of the event or condition that the message reports.
 */
public enum Severity {
    /**
     * Used for messages from the Diagnostic Action Library. Upon enabling diagnostic instrumentation of server and application classes, TRACE messages follow the request path of a method.
     */
    TRACE,
    /**
     * A debug message was generated.
     */
    DEBUG,
    /**
     * Used for reporting normal operations; a low-level informational message.
     */
    INFO,
    /**
     * An informational message with a higher level of importance.
     */
    NOTICE,
    /**
     * A suspicious operation or configuration has occurred but it might not affect normal operation.
     */
    WARNING,
    /**
     * A user error has occurred. The system or application can handle the error with no interruption and limited degradation of service.
     */
    ERROR,
    /**
     * A system or service error has occurred. The system can recover but there might be a momentary loss or permanent degradation of service.
     */
    CRITICAL,
    /**
     * A particular service is in an unusable state while other parts of the system continue to function. Automatic recovery is not possible; the immediate attention of the administrator is needed to resolve the problem.
     */
    ALERT,
    /**
     * The server is in an unusable state. This severity indicates a severe system failure or panic.
     */
    EMERGENCY
}
