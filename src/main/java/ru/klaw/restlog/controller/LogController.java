package ru.klaw.restlog.controller;

import io.swagger.annotations.*;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.klaw.restlog.model.Log;
import ru.klaw.restlog.model.LogFilter;
import ru.klaw.restlog.service.LogFileParser;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

@RestController
@Api(tags="Log Controller")
public class LogController {
    private final LogFileParser logFileParser;

    public LogController(LogFileParser logFileParser) {
        this.logFileParser = logFileParser;
    }

    @ApiOperation(value = "Поиск логов на сервере WebLogic")
    @PostMapping(path = "/getLog", consumes = {"application/json"}, produces = {"application/json"})
    public Log[] getLog(
            @ApiParam("Количество возращаемых результатов.")
            @RequestParam(defaultValue = "100") @Valid @Min(1) @Max(1000) int maxResultLength,
            @ApiParam("Пропуск первых N строк")
            @RequestParam(defaultValue = "0") @Valid @Positive @Max(10000) int skipLine,
            @ApiParam("Общее количество строк после чтения которых будет возращен результат.")
            @RequestParam(defaultValue = "5000") @Valid @Positive @Max(10000) int lineRead,
            @ApiParam("Указывает имя сервера, с которого будут возращены результаты")
            @RequestParam(required = false) String serverName,
            @ApiParam("Время, после которого будут возвращены результаты. Формат 'yyyy-MM-dd'T'HH:mm:ss.SSSXXX'.")
            @RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime after,
            @ApiParam("Фильтрует логи, каждый параметр проверяет содержиться ли подстрока в указанном сегменте лога. " +
                    "Кроме параметра 'severity', который возвращает все логи с равным указанному параметру или выше.")
            @RequestBody(required = false) LogFilter filter) {
        return logFileParser.findLog(filter, serverName, maxResultLength, skipLine, lineRead, after);
    }
}