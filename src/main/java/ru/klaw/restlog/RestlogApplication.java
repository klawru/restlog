package ru.klaw.restlog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.ws.config.annotation.EnableWs;

@SpringBootApplication
@EnableWs
public class RestlogApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestlogApplication.class, args);
	}

}
