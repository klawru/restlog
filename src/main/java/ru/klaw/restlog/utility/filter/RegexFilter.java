package ru.klaw.restlog.utility.filter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public abstract class RegexFilter<T> implements Filter<T> {
    final Matcher pattern;

    public RegexFilter(String pattern) {
        this.pattern = Pattern.compile(pattern).matcher("");
    }

    protected boolean find(String line){
        pattern.reset(line);
        return pattern.find();
    }
}
